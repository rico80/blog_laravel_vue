@extends('layouts.app')

@section('content')
    <pagina tamanho="12">
        {{--<painel titulo="Paineis">
            <div class="col-sm-6 col-md-4">
                <painel titulo="P1" cor="blue">Conteudo 1</painel>
            </div>
            <div class="col-sm-6 col-md-4">
                <painel titulo="Conteudo 2" cor="panel-info">Conteudo 2</painel>
            </div>
            <div class="col-sm-6 col-md-4">
                <painel titulo="Conteudo 3" cor="panel-danger">Conteudo 3</painel>
            </div>
        </painel>--}}
        <painel titulo="Caixas">
            <migalhas :lista="{{$listaMigalhas}}"></migalhas>
            <div class="col-sx-6 col-sm-4 col-md-4">
                <caixa titulo="Artigos" quantidade="125" cor="green" url="{{ route("artigos.index") }}" icone="ion ion-pie-graph"></caixa>
            </div>
            <div class="col-sx-6 col-sm-4 col-md-4">
                <caixa titulo="Usuários" quantidade="45" cor="purple" url="#" icone="ion ion-person"></caixa>
            </div>
            <div class="col-sx-6 col-sm-4 col-md-4">
                <caixa titulo="Autores" quantidade=12 cor="orange" url="#" icone="ion ion-person-stalker"></caixa>
            </div>
        </painel>
    </pagina>
@endsection
