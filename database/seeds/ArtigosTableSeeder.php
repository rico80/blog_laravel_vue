<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ArtigosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artigos')->insert([
            [
                'titulo' => 'PHP OO',
                'descricao' => 'Curso de PHP OO',
                'conteudo' => 'Curso de PHP OO',
                'data' => Carbon::create('2019', '12', '4')
            ],
            [
                'titulo' => 'React',
                'descricao' => 'Curso de React',
                'conteudo' => 'Curso de React',
                'data' => Carbon::create('2019', '12', '4')
            ],
            [
                'titulo' => 'VueJS',
                'descricao' => 'Curso de VueJS',
                'conteudo' => 'Curso de VueJS',
                'data' => Carbon::create('2019', '12', '4')            ]
        ]);
    }
}
